#!/bin/bash

export ANDROID_SDK_ROOT=/opt/android-sdk/

cd android-app
git pull

rm -rf app/build/outputs

{
  ./gradlew assembleRelease --offline
} || {
  ./gradlew assembleRelease
}
rm -rf /output/*
mkdir -p /output

cp -r -v app/build/outputs/* /output

cp privacy_policy_config.toml /output || true

commit_path=/output/COMMIT_MSG.txt
echo "Last commit message:"   | tee -a $commit_path
echo "-----------------------------------------------------------------------"  | tee -a $commit_path
echo -n "Current branch : " | tee -a $commit_path
git rev-parse --abbrev-ref HEAD | tee -a $commit_path
git log -1 --stat | head -n 20  | tee -a $commit_path
echo "-----------------------------------------------------------------------"  | tee -a $commit_path
